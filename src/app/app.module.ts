import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";

import { ListeResultatsComponent } from "./components/liste-resultats/liste-resultats.component";
import { HttpClientModule } from "@angular/common/http";

import { PaginationComponent } from "./components/liste-resultats/pagination/pagination.component";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [AppComponent, ListeResultatsComponent, PaginationComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
