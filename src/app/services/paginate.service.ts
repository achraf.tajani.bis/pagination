import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

interface PostModel {
  userId: number;
  id: number;
  title: string;
  body: string;
}
@Injectable({
  providedIn: "root",
})
export class PaginateService {
  //https://jsonplaceholder.typicode.com/posts?_page=2
  posts: number = 100;
  constructor(private http: HttpClient) {}

  getData(configPagination?): Observable<PostModel[]> {
    if (!configPagination.page) configPagination.page = 1;
    return this.http.get<PostModel[]>(
      "https://jsonplaceholder.typicode.com/posts?_page=" +
        configPagination.page
    );
  }
}
