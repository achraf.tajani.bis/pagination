import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";

@Component({
  selector: "app-pagination",
  templateUrl: "./pagination.component.html",
  styleUrls: ["./pagination.component.css"],
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() configPagination;
  @Output() paginateEvent = new EventEmitter();
  pages: number;
  curentPage: number;

  constructor() {}
  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.configPagination);
  }

  ngOnInit() {
    this.pages = Math.ceil(
      this.configPagination.count / this.configPagination.perPage
    );
  }

  paginate(element: number) {
    console.log(this.configPagination);
    this.paginateEvent.emit(element + 1);
  }
  suivant() {
    console.log(this.configPagination);
    this.paginateEvent.emit("suivant");
  }
  precedent() {
    console.log(this.configPagination);
    this.paginateEvent.emit("precedent");
  }
  first() {
    console.log(this.configPagination);
    this.paginateEvent.emit("first");
  }
  last() {
    console.log(this.configPagination);
    this.paginateEvent.emit("last");
  }
  counter(i: number) {
    return new Array(this.pages);
  }
  onChange($event) {
    if (this.curentPage > 0 && this.curentPage <= this.pages) {
      this.paginateEvent.emit(this.curentPage);
    }
  }
}
