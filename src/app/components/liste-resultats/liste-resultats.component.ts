import { Component, OnInit } from "@angular/core";
import { PaginateService } from "src/app/services/paginate.service";
interface PostModel {
  userId: number;
  id: number;
  title: string;
  body: string;
}
@Component({
  selector: "app-liste-resultats",
  templateUrl: "./liste-resultats.component.html",
  styleUrls: ["./liste-resultats.component.css"],
})
export class ListeResultatsComponent implements OnInit {
  posts: PostModel[];
  pages: number;
  configPagination = {
    count: 100,
    page: 1,
    perPage: 10,
    first: false,
    last: false,
  };
  constructor(private paginateService: PaginateService) {
    console.log("service up");
    this.pages = Math.ceil(
      this.configPagination.count / this.configPagination.perPage
    );
  }
  ngOnInit(): void {
    this.retriveData();
  }

  paginate(event) {
    this.resetPagination();
    switch (event) {
      case "first":
        this.configPagination.first = true;
        this.configPagination.page = 1;
        break;
      case "precedent":
        if (this.configPagination.page - 1 >= 1)
          this.configPagination.page = this.configPagination.page - 1;
        else this.configPagination.first = true;

        break;
      case "suivant":
        if (this.configPagination.page < this.pages)
          this.configPagination.page = this.configPagination.page + 1;
        else this.configPagination.last = true;

        break;
      case "last":
        this.configPagination.last = true;
        this.configPagination.page = this.pages;
        break;
      default:
        this.configPagination.page = event;
        break;
    }
    this.retriveData();
  }
  resetPagination() {
    this.configPagination.first = false;
    this.configPagination.last = false;
  }
  retriveData() {
    this.paginateService.getData(this.configPagination).subscribe((d) => {
      this.posts = d;
    });
  }
}
